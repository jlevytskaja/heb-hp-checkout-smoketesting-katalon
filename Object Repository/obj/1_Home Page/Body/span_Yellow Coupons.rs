<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Yellow Coupons</name>
   <tag></tag>
   <elementGuidId>eb46df8c-4b62-47fa-b32f-31853905cdd0</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yellow Coupons</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;widget block block-static-block&quot;]/div[@class=&quot;quickshop container&quot;]/div[@class=&quot;tags&quot;]/ul[@class=&quot;tags-wrapper&quot;]/li[@class=&quot;tag coupons&quot;]/a[@class=&quot;coupons&quot;]/span[@class=&quot;info&quot;]/span[1]</value>
   </webElementProperties>
</WebElementEntity>
