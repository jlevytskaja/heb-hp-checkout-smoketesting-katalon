<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Meal_Simple</name>
   <tag></tag>
   <elementGuidId>6f2fd6ee-8be7-4d6b-80db-df750f8af982</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = 'https://qa.cert.hebtoyou.net/catalog/category/view/s/meal-simple/id/2112/']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/catalog/category/view/s/meal-simple/id/2112/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-label</name>
      <type>Main</type>
      <value>Meal Simple</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;widget block block-static-block&quot;]/div[@class=&quot;container featured-container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-lg-4&quot;]/div[@class=&quot;slider-item&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
