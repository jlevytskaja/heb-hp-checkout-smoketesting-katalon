<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>aside_Close</name>
   <tag></tag>
   <elementGuidId>55729056-2ff4-4554-9438-b2793d173108</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>aside</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>dialog</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-popup login_modal                modal-slide                _inner-scroll _show</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>modal-content-21</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-role</name>
      <type>Main</type>
      <value>modal</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-type</name>
      <type>Main</type>
      <value>popup</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
    
        
            
            
                Close
            
        
        
    
        
            
                Login
            
            
                Register
            
            
                forgotpassword
            
        
        
            
                
    
    
    
        
        Login
        
            
            
            
                
                    
                    
                
                
                
                
                    
                        
                        
                    
                    
                        
                        
                    
                    
                        
                    
                        
                        
                        
                        
                    
                    
                        
                    
                Forgot your password?
                
                
            
            
                
                
            
            
            
        
    
                
            // add Raas login screens function to Gigya init.
            window.gigyaInit = window.gigyaInit || [];
            var screenSetParams = {
                screenSet : &quot;UAT-RegistrationLogin&quot;,
                containerID : &quot;gigya-login-embed&quot;,
                startScreen : &quot;gigya-login-screen&quot;,
                mobileScreenSet : &quot;&quot; // optional - for using separate mobile screen set.
            };
            var raasLoginScreen = {&quot;function&quot;: &quot;accounts.showScreenSet&quot;, &quot;parameters&quot;: screenSetParams};
            window.gigyaInit.push(raasLoginScreen);
        
    

            
    
            
            
                
Registration
        
            
            
                
                    
                    
                    
                        
                        
                        
                    
                    
                
                
                
                
                    
                        
                            
                                
                                
                            
                        
                    
                                
                                
                            
                                
                                
                                                                 Get SMS order status alerts from HEB2YOU*           Message and data rates may apply. Message frequency may vary. Text STOP to 99147 to cancel. Text HELP to 99147 for help or call Customer Service at 1-800-432-3113. Full terms and privacy at https://hebtoyou.com/sms_terms_99147/Get exclusive offers and savings from HEB.          * 
                    
                        
                        
                        
                        
                    
                    
                        
                        
                        
                        
                        
                        
                        
                        
                    
                    
                        
                    
                        
                    
                
            
            
                
                
            
        
    

    window.gigyaInit = window.gigyaInit || [];
    var screenSetParams = {
        screenSet : &quot;UAT-RegistrationLogin&quot;,
        containerID : &quot;gigya-register&quot;,
        startScreen : &quot;gigya-register-screen&quot;,
        mobileScreenSet : &quot;&quot; // optional - for using separate mobile screen set.
    };
    var raasRegisterScreen = {&quot;function&quot;: &quot;accounts.showScreenSet&quot;, &quot;parameters&quot;: screenSetParams};
    window.gigyaInit.push(raasRegisterScreen);

            
        
    


        
    
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column nav-sticky logining _has-modal&quot;]/div[@class=&quot;modals-wrapper&quot;]/aside[@class=&quot;modal-popup login_modal                modal-slide                _inner-scroll _show&quot;]</value>
   </webElementProperties>
</WebElementEntity>
