<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Dairy and Frozen</name>
   <tag></tag>
   <elementGuidId>3284df6e-68b1-4a74-a3b3-3ed7db6d5031</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>aisle__link</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/catalog/category/view/s/dairy-and-frozen/id/1970/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                Dairy and Frozen                            
                        </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;homepage-aisle&quot;]/div[@class=&quot;aisle-wrapper&quot;]/div[@class=&quot;row visible-desktop text-center&quot;]/div[@class=&quot;col-xs-12 col-sm-6 col-lg-3&quot;]/div[@class=&quot;aisle-item&quot;]/a[@class=&quot;aisle__link&quot;]</value>
   </webElementProperties>
</WebElementEntity>
