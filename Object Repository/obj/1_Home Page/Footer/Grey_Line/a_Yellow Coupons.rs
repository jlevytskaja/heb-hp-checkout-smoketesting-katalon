<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Yellow Coupons</name>
   <tag></tag>
   <elementGuidId>ed8526a6-057b-49e9-bd55-232f1f423617</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/shop/coupons.html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Yellow Coupons</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column nav-sticky&quot;]/div[@class=&quot;page-wrapper&quot;]/footer[@class=&quot;page-footer&quot;]/div[@class=&quot;footer content&quot;]/div[@class=&quot;footer_top&quot;]/ul[@class=&quot;footer_top_links&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
