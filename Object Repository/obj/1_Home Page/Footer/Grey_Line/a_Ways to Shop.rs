<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Ways to Shop</name>
   <tag></tag>
   <elementGuidId>29dc7960-087a-4a9e-b88a-26f41052799d</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/shop.html</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Ways to Shop</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column nav-sticky&quot;]/div[@class=&quot;page-wrapper&quot;]/footer[@class=&quot;page-footer&quot;]/div[@class=&quot;footer content&quot;]/div[@class=&quot;footer_top&quot;]/ul[@class=&quot;footer_top_links&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
