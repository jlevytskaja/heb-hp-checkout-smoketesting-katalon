<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Privacy Policy</name>
   <tag></tag>
   <elementGuidId>29162c2f-0685-424a-be04-0bff80876982</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/privacy-policy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Privacy Policy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column nav-sticky&quot;]/div[@class=&quot;page-wrapper&quot;]/footer[@class=&quot;page-footer&quot;]/div[@class=&quot;footer content&quot;]/div[@class=&quot;footer_bottom&quot;]/ul[@class=&quot;footer_bottom_links&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
