<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Contact Us</name>
   <tag></tag>
   <elementGuidId>e1f17d59-f114-4dd8-928b-e9f879eff355</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column nav-sticky&quot;]/div[@class=&quot;page-wrapper&quot;]/footer[@class=&quot;page-footer&quot;]/div[@class=&quot;footer content&quot;]/div[@class=&quot;footer_bottom&quot;]/ul[@class=&quot;footer_bottom_links&quot;]/li[@class=&quot;item&quot;]/a[1][count(. | //a[@href = 'https://qa.cert.hebtoyou.net/contact-us' and (text() = 'Contact Us' or . = 'Contact Us')]) = count(//a[@href = 'https://qa.cert.hebtoyou.net/contact-us' and (text() = 'Contact Us' or . = 'Contact Us')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/contact-us</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Contact Us</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;cms-home cms-index-index page-layout-1column nav-sticky&quot;]/div[@class=&quot;page-wrapper&quot;]/footer[@class=&quot;page-footer&quot;]/div[@class=&quot;footer content&quot;]/div[@class=&quot;footer_bottom&quot;]/ul[@class=&quot;footer_bottom_links&quot;]/li[@class=&quot;item&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
