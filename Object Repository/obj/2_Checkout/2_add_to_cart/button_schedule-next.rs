<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_schedule-next</name>
   <tag></tag>
   <elementGuidId>85ff7e06-5e58-4b10-b66f-ce8d828e7aec</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='shipping-method-buttons-container']//div[@class='primary']//button[@data-role='opc-continue']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
