<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_card select-stored-card</name>
   <tag></tag>
   <elementGuidId>13cab63b-99d1-4778-a057-c55b438903f5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card select-stored-card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>click: $parent.selectStoredCard.bind(this, $parent), css: $parent.selectedCardHash() == paymentAlias ? 'selected' : ''</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;items payment-methods&quot;]/div[@class=&quot;payment-group&quot;]/ul[@class=&quot;payment-method _active&quot;]/div[@class=&quot;stored-cards-wrapper&quot;]/ul[@class=&quot;stored-cards-list&quot;]/div[1]/li[@class=&quot;card stored-card&quot;]/span[@class=&quot;card select-stored-card&quot;]</value>
   </webElementProperties>
</WebElementEntity>
