<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Order Details</name>
   <tag></tag>
   <elementGuidId>da0e56d8-e56a-4602-aea9-b9cad510c66a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://qa.cert.hebtoyou.net/sales/order/view/order_id/372/</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Order Details</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;maincontent&quot;)/div[@class=&quot;columns&quot;]/div[@class=&quot;column main&quot;]/div[@class=&quot;checkout-success&quot;]/div[@class=&quot;checkout_success_bottom&quot;]/div[@class=&quot;order_info_wrapper&quot;]/div[@class=&quot;order_info&quot;]/div[@class=&quot;info_item order_details&quot;]/span[@class=&quot;order_details&quot;]/a[1]</value>
   </webElementProperties>
</WebElementEntity>
