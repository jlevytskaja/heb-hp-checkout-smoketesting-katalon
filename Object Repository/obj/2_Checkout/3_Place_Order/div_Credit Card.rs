<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Credit Card</name>
   <tag></tag>
   <elementGuidId>d5409f1a-a422-4077-aff8-79ac1786bdf5</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-bind</name>
      <type>Main</type>
      <value>text: 'Credit Card'</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Credit Card</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkout-payment-method-load&quot;)/div[@class=&quot;items payment-methods&quot;]/div[@class=&quot;payment-group&quot;]/ul[@class=&quot;payment-method _active&quot;]/ul[@class=&quot;stored-cards&quot;]/li[@class=&quot;new-card&quot;]/div[@class=&quot;label&quot;]</value>
   </webElementProperties>
</WebElementEntity>
