<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>1_Verifying Top_menu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-26T17:53:38</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>a12150c3-ade3-475a-bff6-871686d4d31e</testSuiteGuid>
   <testCaseLink>
      <guid>1af2ac75-0367-4930-9cac-fce87fa3282a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ccf97511-e5a4-4891-a1a2-b92553af8729</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/2_ Store_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1149c16f-4e35-4c3d-aa51-511303b1d455</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/1_Verifying Top_Menu/1_Verify that user could Close_banner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d9e8e86-2ab2-4445-8316-e0455a916e6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/1_Verifying Top_Menu/2_Verify that user could Click_Weekly_Ad</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0b05808-daa5-41b7-8408-636393d618c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/1_Verifying Top_Menu/4_Verify that user couldClick_Yellow_Coupons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4587afef-65da-46ed-b708-4933055ee24b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/1_Verifying Top_Menu/5_Verify that user could Click_LoginRegister</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
