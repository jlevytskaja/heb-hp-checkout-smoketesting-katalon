<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2_Verifying Grey_line</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-05T17:58:44</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>306c52d9-8797-4d35-916f-64d845a1d486</testSuiteGuid>
   <testCaseLink>
      <guid>b3a9c2c5-ea05-4c49-96fa-ff0c513b2174</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b404ac4b-2c6d-47ff-8351-6db9925e238d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/2_ Store_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f4ac8c0-60b2-4ee7-a66e-3699034b070c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/2_Verifying Grey_Line/1_Verify that user could Click_shop-assistant-switcher</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a5a5d29-5353-413d-b74a-382faf25a6e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/2_Verifying Grey_Line/2_Verify that user could Click_crubside_Pickup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5898647d-77d7-4e6c-9c43-bff44b46b04f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/2_Verifying Grey_Line/3_Verify that user could Click_change_Shop</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17da0430-fe26-4921-8ba9-861ee750876b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/2_Verifying Grey_Line/4_Verify that user could Click_Reserve-Time-Slot</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ed40e33-d1d8-49f2-8190-aceddcb4c045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/2_Verifying Grey_Line/5_Verify that user could Search</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e30c202-c642-4aeb-b75b-d7999ea5425c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/2_Verifying Grey_Line/6_Verify that user could Click_Cart</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
