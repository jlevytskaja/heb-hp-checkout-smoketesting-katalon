<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>3_Verifying Category Menu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-06-27T17:15:17</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>71389f96-e549-4700-83c0-e58d0299afb7</testSuiteGuid>
   <testCaseLink>
      <guid>1074831e-a4b9-4344-9e66-d49727d31e92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24ce75fd-d231-4118-ab8f-c5b0f01ae40f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/2_ Store_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6940a1a2-bdd1-46e3-b247-6869835c93d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/1_Verifying Header/3_Verifying Category_Menu/Verify that user could Click_categories</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f13a46d6-cf23-4478-8f02-e27f0617a092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/Back_to_Home_Page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
