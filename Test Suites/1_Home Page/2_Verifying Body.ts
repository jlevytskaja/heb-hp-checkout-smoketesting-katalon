<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>2_Verifying Body</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-05T14:38:41</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>85fc1b4e-8753-4411-b52a-f08feb45deac</testSuiteGuid>
   <testCaseLink>
      <guid>53965c38-6815-43f5-a236-c0ff8ea1134d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>861084ce-b8b5-49fe-99a0-4cb5840cd266</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/2_ Store_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>662136e6-2ebc-449d-8a79-f4d28e9705d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/1_Verify that user could Click_Banners</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e23522c0-253a-491f-a012-5a8fe87c7228</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/2_Verify that user could Click_Explore_More</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1798d555-d6ef-43c6-8f74-6a439e1d058e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/3_Verify that user could Click_Products_On_Sale</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe1b7384-ba5d-46d3-abda-15f39de2f8e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/4_Verify that user could Click_Organic_Products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>003a159e-72b5-498d-b2ad-4b8bed84a4b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/5_Verify that user could Click_New_Products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fb6c1d5-96c7-486f-b378-8a4585adbde8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/6_Verify that user could Click_Yellow_Coupons</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52eca99c-007a-4d84-bf1e-c64f31cc1f8b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/7_Verify that user could Click_Gluten_Free_Products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7034951-7cbe-46e4-a7ff-5cdce35e41d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/8_Verify that user could Click_Previously_Purshased</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28312101-13ec-4371-9f63-84faa110c383</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/1_Home Page/2_Verifying Body/9_Verify that user could Click_Local_Products</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
