<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login_addtocart_checkout_withoutYC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-07-09T13:49:19</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>66286070-0d4d-45e0-8672-05423f3047c4</testSuiteGuid>
   <testCaseLink>
      <guid>fff82545-570f-46d2-8230-1f577344afb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/1_Open_Index</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03553f5f-1381-4dc6-8a3d-526507776059</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/2_ Store_Select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4368cb4-c8f3-493a-b1bc-ba350eeadce3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/0_GIVEN/3_User/1_Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f67dc7f0-4ba8-4412-98d9-8d846d84061d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_Checkout/1_Add_to_cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>210829a9-447e-43bd-8116-6c59b839e003</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_Checkout/2_Review_cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36c9983d-8569-47a9-98f8-0d788caabab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_Checkout/3_Scedule-Next</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7883cdd3-0d89-4e19-a6c9-32e11d500dfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_Checkout/4_Schedule-Time select</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e80b940c-2617-413b-bc47-9e8edeb96bcb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_Checkout/5_Scedule_Confirm</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f17a5661-5b06-4e1c-8650-bc59297ed197</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/2_Checkout/6_Continue</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
