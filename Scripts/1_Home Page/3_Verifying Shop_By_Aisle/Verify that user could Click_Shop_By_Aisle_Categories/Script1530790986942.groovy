import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Fruit  Vegetables'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Fruit  Vegetables'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Meat and Seafood'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Meat and Seafood'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Baby and Toys'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Baby and Toys'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Dairy and Frozen'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Dairy and Frozen'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Drinks Beer and Wine'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Drinks Beer and Wine'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Deli and Bakery'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Deli and Bakery'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Food to Go'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Food to Go'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Health and Beauty'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Health and Beauty'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Pantry'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Pantry'))

WebUI.click(findTestObject('null'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/a_Household and Kitchen'))

WebUI.click(findTestObject('obj/1_Home Page/ShopByAisle/h1_Household and Kitchen'))

WebUI.click(findTestObject('null'))

WebUI.closeBrowser()

