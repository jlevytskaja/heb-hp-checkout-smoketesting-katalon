import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.click(findTestObject('obj/1_Home Page/Header/3_Category_Menu/span_Baby and Toys'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/a_Safety'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_add-to-cart-CLP'))

WebUI.click(findTestObject('obj/1_Home Page/Header/2_Grey_Line/button_show_cart'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_Continue'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_payment-place-order'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_review-cart-continue'))

WebUI.scrollToPosition(1400, 980)

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_schedule-next'))

WebUI.scrollToPosition(1400, 980)

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_schedule-next'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_time-slot-select'))

WebUI.scrollToElement(findTestObject('obj/2_Checkout/2_add_to_cart/button_review-schedule-next-confirm'), 0)

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_review-schedule-next-confirm'))

WebUI.click(findTestObject('obj/2_Checkout/2_add_to_cart/button_unclipped-coupon-continue'))

WebUI.openBrowser('')

WebUI.click(findTestObject('obj/2_Checkout/button_Next'))

WebUI.click(findTestObject('obj/2_Checkout/button_Next_1'))

WebUI.closeBrowser()

