import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.waitForElementPresent(findTestObject('obj/1_Login/Login'), 20)

WebUI.click(findTestObject('obj/1_Login/Login'))

WebUI.waitForElementPresent(findTestObject('obj/1_Login/gigya-login-form'), 20)

WebUI.focus(findTestObject('obj/1_Login/gigya-login-form'))

WebUI.click(findTestObject('obj/1_Login/input_username-login'))

WebUI.setText(findTestObject('obj/1_Login/input_username-login'), 'jlevytskaja128@robofirm.com')

WebUI.click(findTestObject('obj/1_Login/input_password-login'))

WebUI.setText(findTestObject('obj/1_Login/input_password-login'), 'lhfpJngjk_xxy7')

WebUI.click(findTestObject('obj/1_Login/input_gigya-input-submit'))

WebUI.delay(10)

